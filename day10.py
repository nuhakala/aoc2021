from functools import reduce
from operator import add


with open ('./inputs/day10_in.txt', 'r') as file:
    lines = [line.replace("\n", "") for line in file.readlines()]

opening = ['(', '[', '{', '<']
closing = [')', ']', '}', '>']
brackets = {')': '(', ']': '[', '}': '{', '>': '<'}
points = {')': 3, ']': 57, '}': 1197, '>': 25137}
illegal = []
lines_without_corrupted = lines.copy()
for line in lines:
    openBrackets = [')']
    # print(line)
    for char in line:
        if char in opening:
            openBrackets.append(char)
            # print("lisätty")
            # print(char)
        elif brackets[char] == openBrackets[-1]:
            openBrackets.pop()
            # print("poistettu")
            # print(char)
        else:
            illegal.append(char)
            # print("paskaa")
            # print(char)
            # print(openBrackets)
            lines_without_corrupted.remove(line)
            break


res = [points[item] for item in illegal]
# print(len(illegal))
# print(len(lines))
sum = reduce(add, res)
print("viallisten summa: ", end="")
print(sum)
# print(len(lines_without_corrupted))

 # 629892 liian korkea

# *****
# osa kaksi
# *****

# enumLines = enumerate(lines)
closingBrackets = []
reverse_brackets = {'(': ')', '[': ']', '{': '}', '<': '>'}
for line in lines_without_corrupted:
    openBrackets = []
    close = []
    for char in line:
        if char in opening:
            openBrackets.append(char)
        elif brackets[char] == openBrackets[-1]:
            openBrackets.pop()
    for bracket in openBrackets:
        close.insert(0, reverse_brackets[bracket])
    closingBrackets.append(close)


new_points = {')': 1, ']': 2, '}': 3, '>': 4}
sums = []
# print(closingBrackets)
for array in closingBrackets:
    # a = [new_points[item] for item in array]
    middle_sum = 0
    for item in array:
        middle_sum *= 5
        aoeu = new_points[item]
        middle_sum += aoeu
    sums.append(middle_sum)

sums.sort()
middle = int(len(sums) / 2)
print("kesken-eräisten keskimmäinen: ", end="")
print(sums[middle])

# res2 = [new_points[item] for array in closingBrackets for item in array]

