import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day1Second {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day1_in.txt")
    val lines = new BufferedReader(file)

    var eka = lines.readLine().toInt
    var toka = lines.readLine().toInt
    var kolmas = lines.readLine().toInt
    var count = 0

    var sum = eka + toka + kolmas
    var prev = sum
    var next = lines.readLine().toInt

    while (next > 0) {
      sum += next
      sum -= eka
      if (sum > prev) count += 1
      eka = toka
      toka = kolmas
      kolmas = next
      val jotai = lines.readLine()
      if (jotai == null) next = 0
      else next = jotai.toInt
      prev = sum
    }

    file.close()
    lines.close()

    println(count)
  }
}
