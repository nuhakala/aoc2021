import numpy as np

with open ('./inputs/day13_in.txt', 'r') as tiedosto:
    DATA = tiedosto.readlines()

# tavallaan vähän turhaan otettiin koordinaatit tos ylempänä kun kuitenkin käydään loopissa kaikki läpi
foldit = []
koordinaatit = []
for rivi in DATA:
    if "fold" in rivi:
        pituus = len(rivi)
        inputti = rivi[11:pituus].strip()
        rivinumero = int(inputti[2:])
        foldit.append((inputti[0], rivinumero))
    elif "," in rivi:
        pari = tuple(rivi.strip().split(","))
        x = int(pari[0])
        y = int(pari[1])
        koordinaatit.append((x, y))

def ytaitos(rivi, lista):
    palautus = lista.copy()
    for (x, y) in lista:
        if y > rivi:
            erotus = y - rivi
            palautus.remove((x, y))
            palautus.append((x, rivi - erotus))
    return list(set(palautus))

def xtaitos(rivi, lista):
    palautus = lista.copy()
    for (x, y) in lista:
        if x > rivi:
            erotus = x - rivi
            palautus.remove((x, y))
            palautus.append((rivi - erotus, y))
    return list(set(palautus))

eka = []
ekafold = foldit[0]
if ekafold[0] == "y":
    eka = ytaitos(ekafold[1], koordinaatit)
else:
    eka = xtaitos(ekafold[1], koordinaatit)

print("ekan tehtävän vastaus: ", len(eka))

# *******
# toinen tehtävä
# *******

toka = koordinaatit.copy()
for taitos in foldit:
    if taitos[0] == "y":
        toka = ytaitos(taitos[1], toka)
    else:
        toka = xtaitos(taitos[1], toka)

# tehdään 2d array numpyllä ja alustetaan se nollilla, sen jälkeen laitetaan ykkösiks ne
# paikat missä on alkio
# lopputulos ei oo kauheen kaunis ja näyttää vähän joltai peilikuvalta, mutta tuli oikea vastaus
# joten aivan yks hailee
print(toka)
nollat = np.zeros((40, 6))
for alkio in toka:
    nollat[alkio[0], alkio[1]] = 1

print(nollat)
# oikea vastaus on CPJBERUL siltä varalta että on tulevaisuudessa vaikea tulkita tulosta
