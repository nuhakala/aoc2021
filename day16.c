/* Meikän eka c-llä kirjoitettu ohjelma.
 * Ei ole mitenkään erityisen kaunis, tuskin tehokaskaan, mutta viimein
 * varmaan jonku 20h jälkeen sain tän toimimaan. */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#define PITUUS 1393
#define BPITUUS 4 * PITUUS

/* Funktiot */
void muuta(char bin[], char kirjain);
long paketti(char bin[], int *index, int *versio);
int lue(char bin[], int size);
char * lue2(char bin[]);
int lueVakio(char bin[]);
char * lueBinaarit(char bin[], int size);
long binstr2ul(const char *s);


int main() {
  FILE *fp;
  /* me nyt vaan satutaan tietään että inputti on 1393 pitkä */
  char buff[PITUUS + 1] = {0};
  fp = fopen("./inputs/day16_in.txt", "r");

  fgets(buff, PITUUS, fp);
  fclose(fp);

  char bin[BPITUUS + 1] = {0};

  /* pituus - 1 koska muuten se tekee kierroksen liikaa, en tarkalleen ottaen tässä kohtaa tajua
   * että miksi */
  for (int i = 0; i < PITUUS - 1; i++) {
     muuta(bin, buff[i]);
  }

  int index = 0;
  int versioSumma = 0;
  long res = paketti(bin, &index, &versioSumma);
  printf("versinumeroiden summa on: %d\n", versioSumma);
  printf("laskutoimituksen lopputulos on: %ld\n", res);

  return 0;
}


/* Lukee yhden kokonaisen paketin.
 * Jöö, tänki olis voinu tehdä aika monella tapaa fiksummin mutta tää on nyt tämmönen. */
long paketti(char *bin, int *index, int *versio) {
  int ver = lue(&bin[*index], 3);
  int type = lue(&bin[*index + 3], 3);
  char typeLength = bin[*index + 6];
  int length;

  /*lisätään versio versiosummaan*/
  *versio += ver;

  long pak[300] = {0};
  for (int i = 0; i < 300; i++) pak[i] = -1;

  /* Käydään pagetit läpitte ja tallennetaan ne pak-taulukkoon myöhempää käsittelyä varten. */
  if (type != 4) {
    if (typeLength == '1') {
      length = lue(&bin[*index + 7], 11);
      *index += 18;
      int i = 0;

      while (i < length) {
        pak[i] = paketti(bin, index, versio);
        i += 1;
      }

    } else {
      length = lue(&bin[*index + 7], 15);
      *index += 22;
      int alku = *index;
      int i = 0;
      while (*index < alku + length) {
        pak[i] = paketti(bin, index, versio);
        i += 1;
      }
    }
  }

  /* julistetaan vakiot joita tarvitaan switchin sisällä */
  char vakio[256] = {0};
  long res = 0;
  int ind = 0;
  long product = 1;
  long min;
  long max = 0;

  switch (type) {
    case 0:
      while (pak[ind] != -1) {
        if (ind > 25) {
        }
        res += pak[ind];
        ind++;
      }
      break;

    case 1:
      while (pak[ind] != -1) {
        product *= pak[ind];
        ind += 1;
      }
      res = product;
      break;

    case 2:
      min = pak[ind];
      ind++;
      while (pak[ind] != -1) {
        if (pak[ind] < min) 
          min = pak[ind];
        ind++;
      }
      res = min;
      break;

    case 3:
      while (pak[ind] != -1) {
        if (pak[ind] > max) 
          max = pak[ind];
        ind++;
      }
      res = max;
      break;

    case 5:
      if (pak[0] > pak[1]) {
        res = 1;
      } else {
        res = 0;
      }
      break;

    case 6:
      if (pak[0] < pak[1]) {
        res = 1;
      } else {
        res = 0;
      }
      break;

    case 7:
      if (pak[0] == pak[1]) {
        res = 1;
      } else {
        res = 0;
      }
      break;

    case 4:
      *index += 1;
      do {
        *index += 5;
        char *luku;
        luku = lue2(&bin[*index + 1]);
        strcat(vakio, luku);
      } while (bin[*index] == '1');

      *index += 5;
      res = binstr2ul(vakio);
      break;
  }

  return res;
}


/* Jöö, tää nyt vähän kopioi tota toista lue funktiota mutta tässä kohtaa ei enää jaksa kiinnostaa.
 * Haluun vaan saada tän jaskan toimimaan enkä jaksa ruveta muokkaan kaikkea muuta jotta tän sais
 * hoidettuu fiksummin. */
char * lue2(char bin[]) {
  static char chunk[4];
  memset( chunk, 0, (4)*sizeof(char) );
  for (int i = 0; i < 4; i++) {
    chunk[i] = bin[i];
  }
  return chunk;
}


/* Lukee bin taulukosta size määrän alkioita, muuttaa luetut alkiot desimaaliluvuksi
 * kun oletus on että, luettu luku on binaariluku. */
int lue(char bin[], int size) {
  char chunk[size + 1];
  memset( chunk, 0, (size + 1)*sizeof(char) );
  for (int i = 0; i < size; i++) {
    chunk[i] = bin[i];
  }
  long testi2 = binstr2ul(chunk);
  return testi2;
}


/* Muuttaa binaaistringit intteihin
 * Kopioitu täältä: https://codereview.stackexchange.com/questions/43256/binary-string-to-integer-and-integer-to-binary-string */
long binstr2ul(const char *s) {
  unsigned long rc;
  for (rc = 0; '\0' != *s; s++) {
      if (rc > (ULONG_MAX/2)) {
          errno = ERANGE;
          return -1;
      } else if ('1' == *s) {
          rc = (rc * 2) + 1;
      } else if ('0' == *s) {
          rc *= 2;
      } else {
          errno = EINVAL;
          return -1;
      }
  }
  /**num = rc;*/
  return rc;
}


/* Tää funktio on kopioitu jostai intter netsin syövereistä, tosin muokkasin sitä ihan
 * inasen sopimaan tarpeisiini.
 * P.S. tän funktion sisennykset on jostai syystä aivan viturallaan. */
void muuta(char bin[], char kirjain) {
  switch(kirjain) {
    case '0':
      strcat(bin, "0000");
      break;
    case '1':
      strcat(bin, "0001");
      break;
    case '2':
      strcat(bin, "0010");
      break;
    case '3':
      strcat(bin, "0011");
      break;
    case '4':
      strcat(bin, "0100");
      break;
    case '5':
      strcat(bin, "0101");
      break;
    case '6':
      strcat(bin, "0110");
      break;
    case '7':
      strcat(bin, "0111");
      break;
    case '8':
      strcat(bin, "1000");
      break;
    case '9':
      strcat(bin, "1001");
      break;
    case 'a':
    case 'A':
      strcat(bin, "1010");
      break;
    case 'b':
    case 'B':
      strcat(bin, "1011");
      break;
    case 'c':
    case 'C':
      strcat(bin, "1100");
      break;
    case 'd':
    case 'D':
      strcat(bin, "1101");
      break;
    case 'e':
    case 'E':
      strcat(bin, "1110");
      break;
    case 'f':
    case 'F':
      strcat(bin, "1111");
      break;
    default:
      printf("Invalid hexadecimal input.\n");
      printf("%x\n", kirjain);
  }
}
