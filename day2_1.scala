import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day2 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day2_in.txt")
    val lines = new BufferedReader(file)

    var next = lines.readLine()
    var horizontal, depth = 0

    while (next != "#end") {
      val split = next.split(' ')
      split(0) match {
        case "up" => 
          depth -= split(1).toInt

        case "down" =>
          depth += split(1).toInt

        case "forward" =>
          horizontal += split(1).toInt
      }
      next = lines.readLine()
    }

    file.close()
    lines.close()

    println("horizontal " + horizontal)
    println("depth " + depth)
    println("tulo " + depth * horizontal)
  }
}
