import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day1 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day1_in.txt")
    val lines = new BufferedReader(file)
    var count = 0
    var prevLine = lines.readLine().toInt
    var line = lines.readLine().toInt

    while(line != 0) {
      if(line > prevLine) count += 1
        prevLine = line
      line = lines.readLine().toInt
    }

    println(count) // printataan lopuksi muuttujan arvo vielä näkyviin
  }
}
