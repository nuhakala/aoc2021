import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

case class Loc(val row: Int, val column: Int, var isLow: Boolean = false, var lowPoint: Option[Loc] = None) {
  def hasLow = lowPoint.isDefined
  override def toString() = {
    s"$row $column"
  }
}

object Day9 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day9_in.txt")
    val lines = new BufferedReader(file)

    val strs = LazyList.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    val locations = input.map(_.toCharArray())
      .map(_.map(_.toInt - 48))
      .map(_.zipWithIndex).zipWithIndex
      .map( f => f._1.map( a => (a._1, Loc(f._2, a._2))))

    file.close()
    lines.close()

    def neighs(loc: Loc): Buffer[(Int, Loc)] = {
      val row                                = loc.row
      val column                             = loc.column
      val res                                = Buffer[(Int, Loc)]()
      if (row > 0) res                      += locations(row - 1)(column)
      if (row < input.size - 1) res         += locations(row + 1)(column)
      if (column > 0) res                   += locations(row)(column - 1)
      if (column < input.head.size - 1) res += locations(row)(column + 1)

      res
    }

    /**
      * Palauttaa annetun parin sisältämässä paikassa olevan alkion lowpointin, ja samalla
      * asettaa kaikkien matkan varrella olevien lowpointin.
      *
      * @return Annetun parin sisältämän paikan lowpoint
      */
    def getLowest(in: (Int, Loc)): Option[Loc] = {
      val (number, loc) = in
      if (number == 9) return Option(Loc(-1, -1))
      val neighbors = neighs(loc)
      val min = neighbors.minBy(_._1)
      var res: Option[Loc] = None

      if (min._1 <= number) { // ei ole lowpointti
        if (min._2.hasLow) { // jos pienimmällä naapurilla on jo lowpointti, tämän lowpointti on sama
          res = min._2.lowPoint
        } else {
          res = getLowest(min._1, min._2)
        }
      } else { // tää on lowpoint
        loc.isLow = true
        loc.lowPoint = Option(loc)
        res = Option(loc)
      }
      loc.lowPoint = res
      res
    }


    // nämä luupit laskee ykkösosion ja alustaa kakkosen
    var riskLevelSum = 0
    var row = 0
    while (row < input.size) {
      var column = 0
      while (column < input.head.size) {
        val elem = input(row)(column).toInt - 48
        val elemLoc = Loc(row, column)
        val neighbors = neighs(elemLoc)
        if (neighbors.forall(elem < _._1)) riskLevelSum += elem + 1

        // kakkosvaiheen alustus
        getLowest(locations(row)(column))
        column += 1
      }
      row += 1
    }

    println("Riskitasosumma on: " + riskLevelSum)

    // *******
    // osa kaksi
    // *******

    val all = Buffer[(Int, Loc)]()
    for(array <- locations) {
      for (tuple <- array) {
        all += tuple
      }
    }

    val result = all.filter(_._1 != 9)
    val lows = all.filter(_._2.isLow)
    val sums = Buffer[Int]()
    for (low <- lows) {
      sums += result.count( f => f._2.lowPoint.get.row == low._2.row && f._2.lowPoint.get.column == low._2.column )
    }

    val product = sums.sorted.takeRight(3).reduce( _ * _ )
    println("kolmen suurimman basinin tulo on: " + product)
  }
}
