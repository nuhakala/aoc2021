with open ('./inputs/day12_in.txt', 'r') as tiedosto:
    DATA = [tuple(rivi.strip().split("-")) for rivi in tiedosto.readlines()]

def tarkista(x): return x.islower() and x != "start" and x != "end"

KAIKKI_PIENET = set(filter(tarkista, {alkio for pari in DATA for alkio in pari}))

def seuraava(nykyinen, vierailtu, kahdesti=None):
    lista = vierailtu.copy()
    if not nykyinen.isupper(): lista.append(nykyinen)
    if kahdesti != None and nykyinen == kahdesti[0]: kahdesti = (kahdesti[0], kahdesti[1] + 1)
    if kahdesti != None and kahdesti[1] < 2 and kahdesti[0] in lista: lista.remove(kahdesti[0])
    if nykyinen == "end":
        return 1
    summa = 0
    for (alku, loppu) in DATA:
        if nykyinen == alku and loppu not in lista:
            summa += seuraava(loppu, lista, kahdesti)
        if nykyinen == loppu and alku not in lista:
            summa += seuraava(alku, lista, kahdesti)
    return summa

# käydään parit läpi, ja aloitetaan toiminta jos pari sisältää start
eka = 0
for (alku, loppu) in DATA:
    if "start" == alku:
        eka += seuraava(loppu, ["start"])
    if "start" == loppu:
        eka += seuraava(alku, ["start"])

# toinen tehtävä: käydään parit läpi ja annetaan joka kierrokselle alkio jossa saa käydä kahdesti
toka = 0
for alkio in KAIKKI_PIENET:
    for (alku, loppu) in DATA:
        if "start" == alku:
            toka += seuraava(loppu, ["start"], (alkio, 0))
        if "start" == loppu:
            toka += seuraava(alku, ["start"], (alkio, 0))


print("kaikki reitit ovat: ", end="")
print(eka)
print("toinen tehtävä: kaikki reitit niin että yhdessä pikkusessa voi käydä kahdesti: ", end="")
# koska toisen tehtävän looppi ottaa ykkös tehtävän vastauksen mukaan joka kerralla, vaikka ei pitäisi
# niin sen takia täytyy ylimääräiset miinustaa pois
print(toka - (len(KAIKKI_PIENET) - 1) * eka)
