import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Buffer

object Day3 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day3_in.txt")
    val lines = new BufferedReader(file)

    val strs = Stream.continually(lines.readLine()).takeWhile(_ != "end")
    val input = strs.toVector

    file.close()
    lines.close()

    val freq = Array.fill(input.head.size)(0)
    var count = 0

  	for(line <- input) {
  		line.zipWithIndex.foreach({case (a, i) => freq(i) += a.toInt - 48})
  		count += 1
  	}

    var gamma = ""
    var epsilon = ""

    for(elem <- freq) {
    	if(elem > count / 2) {
    		gamma += "1" 
    		epsilon += "0"
    	}
    	else {
    		gamma += "0"
    		epsilon += "1"
    	}
    }

    val gammaInt = Integer.parseInt(gamma, 2)
    val epsilonInt = Integer.parseInt(epsilon, 2)

    println(input.last)
    println("gamma binaari: " + gamma)
    println("epsilon binaari: " + epsilon)
    println("gamma normi lukuna: " + gammaInt)
    println("epsilon normi lukuna: " + epsilonInt)
    println("niiden tulo: " + gammaInt * epsilonInt)
    println("*****************")


    // ***********
    // osa kaksi täälä
    // ***********
    var ox = input
    var i = 0
    while (i < input.head.size) {
    	val amount = ox.count(_(i) == '1')
    	val common = if(amount >= ox.size / 2.0) '1' else '0'
    	if (ox.size > 1) ox = ox.filter(_(i) == common)
    	i += 1
    }

    var co = input
    var a = 0
    while (a < input.head.size) {
    	val amount = co.count(_(a) == '1')
    	val common = if(amount >= co.size / 2.0) '0' else '1'
    	if (co.size > 1) co = co.filter(_(a) == common)
    	a += 1
    }

  	val oxy = Integer.parseInt(ox.head, 2)
  	val co2 = Integer.parseInt(co.head, 2)

  	println("happi binaari: " + ox.head)
    println("co2 binaari: " + co.head)
    println("happi normi lukuna: " + oxy)
    println("co2 normi lukuna: " + co2)
    println("niiden tulo: " + oxy * co2)
  }
  // yritetty 3373608 liian iso 
  // 2972336 2952374 liian pieniä
}
