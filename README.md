# Advent of code 2021
These are my solutions to the problems of Advent of Code 2021:

https://adventofcode.com/2021

The codes are not very pretty and for example the variable names are not always
very representative. But after all, these are a solution to one problem with given
input so does it really matter as long as it gets the job done? (P.S. It is really 
nice to make bold assumptions about the input and forget all the possible
error-situations.)

All the files contain a runnable code that can be compiled and executed. It will
print the answer with given input.

My strongest language is scala, but I wanted to learn something new so I started making
these with python. Thats why first days are made with scala and then with python.
