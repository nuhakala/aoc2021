import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Buffer

case class Pair(key: Int, var flag: Boolean)

class Grid(val grid: Vector[Array[Pair]], var hasBingo: Boolean = false) {
  val marked = Buffer[Int]()

  def isBingo: Boolean = {
    var vertical = false
    var horizontal = false

    for(row <- grid if !vertical) {
      vertical = true
      for(pair <- row) {
        vertical = vertical && pair.flag
      }
    }

    var i = 0
    while (i < grid.size && !horizontal) {
      horizontal = true
      for(row <- grid) {
        horizontal = horizontal && row(i).flag
      }
      i += 1
    }
    vertical || horizontal
  }

  def mark(value: Int) = {
    for(row <- grid) {
      for(pair <- row) {
        if(pair.key == value) {
          pair.flag = true
          marked += value
        }
      }
    }
  }

  def sum: Int = {
    val numbers = Buffer[Int]()

    for(row <- grid) {
      for(pair <- row) {
        if(!pair.flag) numbers += pair.key
      }
    }

    numbers.sum
  }
  
  override def toString(): String = {
    var res = "\n"
    for (row <- grid) {
      for (pair <- row) {
        val jotai = if(pair.key < 10) "  " else " "
        val toinen = if(pair.flag) "*" + pair.key else " " + pair.key
        res += jotai + toinen
      }
      res += "\n"
    }
    res + marked.mkString(" ") + "\n"
  }
}


object Day4 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day4_in.txt")
    val lines = new BufferedReader(file)

    val strs = Stream.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    var numbers = input.head.split(",").filter(_.nonEmpty).map(_.toInt)

    val grids = input.tail.filter(_.nonEmpty) // filtteröidään tyhjät rivit pois, jäljelle jää string muotoisia muuttujia
      .map(_.split(" ")) // splitataa string muotoiset rivit yksittäisiksi numeroiksi, jokaisesta rivistä tulee array
      .map( f => f.filter(_.nonEmpty)) // filtteröidään mahdolliset tyhjät pois, joita syntyy jos on kaksi väliä peräkkäin
      .map(_.map(_.toInt)) // muutetaan stringit numeroiksi
      // tässä kohtaa nyt on vektori jossa on array per yksi rivi
      .map(_.map(Pair(_, false))) // lisätään jokaiselle numerolle lippu
      .grouped(5) // tehdään vitosen settejä
      .map(new Grid(_)) // tehdään jokaisesta setistä grid olio
      .zipWithIndex // zipataa gridit indeksin kanssa
      .toBuffer // muutetaan lopuksi vektoriksi

    var isfound = false
    var index = -1
    var a = -1

    while (a < numbers.size - 1 && !isfound) {
      a += 1
      for((grid, i) <- grids if !isfound) {
        grid.mark(numbers(a))
        isfound = grid.isBingo
        if(isfound) {
          index = i
        }
      }
    }

    

    //grids.foreach(println + "\n") // printtaa kaikki gridit
    //println(grids(index))

    println("ensimmäisenä voittava ruudukko:")
    println("gridin indeksi: " + index)
    println("viimeinen arvottu numero: " + numbers(a))
    println("bingo tuli järjestyksessä " + a + " numerolla")

    println("summa on: " + grids(index)._1.sum * numbers(a))
    
    // 6912 liian matala
    // 53350 liian matala
    
    // *******
    // OSA KAKSI
    // *******


    index = -1
    a = -1
    var counter = 0

    while (a < numbers.size - 1 && counter < 100) {
      a += 1
      for((grid, i) <- grids) {
        if(!grid.hasBingo) {
          grid.mark(numbers(a))
          if(grid.isBingo) {
            grid.hasBingo = true
            counter += 1
          }
          if (counter == 100) index = i
        }
      }
    }

    println("\n\nviimeisenä voittava ruudukko:")
    println("gridin indeksi: " + index)
    println("viimeinen arvottu numero: " + numbers(a))
    println("bingo tuli järjestyksessä " + a + " numerolla")

    println("summa on: " + grids(index)._1.sum * numbers(a))

  }
}
