from collections import deque


with open ('./inputs/day14_in.txt', 'r') as tiedosto:
    DATA = tiedosto.readlines()

alku = DATA[0].strip()

saannot = {tuple(rivi.strip().split(" -> ")[0]): rivi.strip().split(" -> ")[1] for rivi in DATA[2:]}
kirjaimet = set(saannot.values())
maarat = {kirjain: 0 for kirjain in kirjaimet }

# palauttaa stringin jota on iteroitu parametrinä annettu määrä
def xaskelta(jono: str, x: int) -> deque[str]:
    edellinen = jono
    uusi = deque(jono)
    uusi_index = 1
    for i in range(x):
        for index in range(1, len(edellinen)):
            uusi.insert(uusi_index, saannot[tuple(edellinen[index - 1:index + 1])])
            uusi_index += 2
        edellinen = list(uusi)
        uusi_index = 1
    return uusi

memot = {}

def laske_maarat(lista: deque[str]):
    res = {}
    for kirjain in kirjaimet:
        res[kirjain] = lista.count(kirjain)
    return res



def laske(jono: str, taso: int, iteraatiot: int, askelet: int) -> dict[str, int]:
    # jos ei pidä enää tehdä iteraatioita palautetaan tyhjää
    # if(iteraatiot < 0): return {}
    # alustetaan memotaso
    if not (memot.get(taso)): memot[taso] = {}
    # lasketaan uusi tulos
    tulos = xaskelta(jono, askelet)
    # print("".join(tulos))
    # jos seuraava taso on viimeinen, niin ei mennä rekursiossa syvemmälle, vaan lasketaan tuloksen
    # määrät
    if (iteraatiot - askelet <= 0):
        tulos.pop()
        return laske_maarat(tulos)

    maarat = {kirjain: 0 for kirjain in kirjaimet}
    for index in range(1, len(tulos)):
        pari = (tulos[index - 1], tulos[index])
        seuraava: dict[str, int] = {}
        if(memot[taso].get(pari)):
            seuraava = memot[taso].get(pari)
        else:
            seuraava = laske("".join(pari), taso + 1, iteraatiot - askelet, askelet)
            memot[taso][pari] = seuraava
        for kirjain in seuraava:
            maarat[kirjain] += seuraava[kirjain]
    return maarat

# ykkös tehtävän vastauksen saa vaihtamalla 40 tilalle 10
tulos = laske(alku, 1, 40, 10)
# pitää korjata lisäämällä yksi, koska rekursion alimmalla tasolla ultimaattisen stringin vika kirjain jää laskematta
# koska oikean tuloksen saamiseksi pitää välilaskuissa jättää välistringin vika kirjain huomiotta
# katso rivi 47
tulos[alku[-1]] += 1

tulos = tulos.values()
maksimi = max(tulos)
minimi = min(tulos)

print("suurin on: ", maksimi)
print("pienin on: ", minimi)
print("suurimman ja pienimmän erotus on: ", maksimi - minimi)
# 346 liian vähän
# 576 liian vähän
# 672 liian vähän
# 1362 väärin
