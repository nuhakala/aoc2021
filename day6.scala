import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day6 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day6_in.txt")
    val lines = new BufferedReader(file)

    var strs = Stream.continually(lines.readLine()).takeWhile(_ != null)
    var input = strs.toVector

    file.close()
    lines.close()

    var initial = input.head.split(",").map(_.toLong).toBuffer
    var groups = initial.groupBy(f => f)
    var amounts = Buffer.fill(9)(0L)
    for ((index, array) <- groups) {
      amounts(index.toInt) = array.size.toLong
    }

    def round(in: Buffer[Long]): Buffer[Long] = {
      val count = in(0)
      val res = in.tail
      res(6) += count
      res += count
    }

    var first = 0L
    var i = 0
    while (i < 256) {
      amounts = round(amounts)
      if (i == 79) first = amounts.sum // 79 because we start from 0, so 79 is the 80th round
      i += 1
    }

    println("Fish population after 80 days: " + first)
    println("Fish population after 256 days: " + amounts.sum)
 }
}
