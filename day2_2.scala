import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day2Second {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day2_in.txt")
    val lines = new BufferedReader(file)

    var next = lines.readLine()
    var horizontal, depth, aim = 0

    while (next != "#end") {
      val split = next.split(' ')
      split(0) match {
        case "up" => 
          aim -= split(1).toInt

        case "down" =>
          aim += split(1).toInt

        case "forward" =>
          val forward = split(1).toInt
          horizontal += forward
          depth += forward * aim
      }
      next = lines.readLine()
    }

    file.close()
    lines.close()

    println("horizontal " + horizontal)
    println("depth " + depth)
    println("tulo " + depth * horizontal)
  }
}
