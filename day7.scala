import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day7 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day7_in.txt")
    val lines = new BufferedReader(file)

    val strs = Stream.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    val numbers = input.head.split(",")
      .map(_.toInt)
      .groupBy(f => f)
      .map(f => (f._1, f._2.size))

    //numbers.foreach(println)

    var i = 0
    val fuels = Array.fill(2000)(0)
    while (i < 2000) {
      for ((position, amount) <- numbers) {
        val fuel = amount * scala.math.abs(i - position)
        fuels(i) += fuel
      }
      i += 1
    }

    println("ensimmäisen tehtävän polttoaineet: " + fuels.min)

    // *******
    // toinen tehtävä alkaa täällä
    // *******

    i = 0
    val fuels2 = Array.fill(2000)(0)
    while (i < 2000) {
      for ((position, amount) <- numbers) {
        val distance = scala.math.abs(i - position)
        val fuel = if(distance > 0) (1 to distance).reduce(_ + _) else 0
        fuels2(i) += fuel * amount
      }
      i += 1
    }

    println("toisen tehtävän polttoaineet: " + fuels2.min)
    val testi = 3
  }
}
