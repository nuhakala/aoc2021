import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Buffer

object Day5 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day5_in.txt")
    val lines = new BufferedReader(file)

    val strs = Stream.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    val matrix = Buffer.fill(1000, 1000)(0)

    def addLine(in: Array[Array[Int]]) = {
      val iStartR = in(0)(0)
      val iStartC = in(0)(1)
      val iEndR = in(1)(0)
      val iEndC = in(1)(1)
      var startC, startR, endC, endR = 0
      if (iStartC < iEndC) {
        startC = iStartC
        endC = iEndC
      } else {
        startC = iEndC
        endC = iStartC
      }
      if (iStartR < iEndR) {
        startR = iStartR
        endR = iEndR
      } else {
        startR = iEndR
        endR = iStartR
      }

      if (startC == endC) {
        val length = scala.math.abs(endR - startR)
        var i = 0

        while (i < length + 1) {
          matrix(i + startR)(startC) += 1
          i += 1
        }
      } 
      if (startR == endR) {
        val length = scala.math.abs(endC - startC)
        var i = 0
        while (i < length + 1) {
          matrix(startR)(i + startC) += 1
          i += 1
        }
      }
    }


    val inputLines = input.filter(_.nonEmpty) // filtteröidään tyhjät rivit pois, jäljelle jää string muotoisia muuttujia
      .map(_.split(" -> ")) // splitataa string muotoiset rivit yksittäisiksi numeroiksi, jokaisesta rivistä tulee array
      .map(_.map(_.split(",")))
      .map(_.map(_.map(_.toInt))) // muutetaan stringit numeroiksi

    inputLines.foreach(addLine(_))
    val result = matrix.map(_.count(_ > 1))

    println("määrä on " + result.sum)
    // 522 liian matala
    // 1536 liian vähän
    // 6529 liian vähän
  }
}
