import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Buffer

object Day5Second {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day5_in.txt")
    val lines = new BufferedReader(file)

    val strs = Stream.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    val matrix = Buffer.fill(1000, 1000)(0)

    def addLine(in: Array[Array[Int]]) = {
      val startR = in(0)(0)
      val startC = in(0)(1)
      val endR = in(1)(0)
      val endC = in(1)(1)

      if (startC == endC) {
        val length = endR - startR // scala.math.abs(endR - startR)
        var i = 0

        // if legth < 0 -> the end position is smaller and we start from it
        if (length < 0) {
          while (i < -length + 1) {
            matrix(i + endR)(startC) += 1
            i += 1
          }
        } else {
          while (i < length + 1) {
            matrix(i + startR)(startC) += 1
            i += 1
          }
        }
      }

      if (startR == endR) {
        val length = endC - startC
        var i = 0

        if (length < 0) {
          while (i < -length + 1) {
            matrix(startR)(i + endC) += 1
            i += 1
          }
        } else {
          while (i < length + 1) {
            matrix(startR)(i + startC) += 1
            i += 1
          }
        }
      }

      if (startR != endR && startC != endC) {
        val length = scala.math.abs(endC - startC)
        val verLength = (endC - startC) / length 
        val horLength = (endR - startR) / length // we scale these to the length of 1 so that we only get the direction

        var i = 0
        while (i < length + 1) {
          matrix(startR + i * horLength)(startC + i * verLength) += 1
          i += 1
        }
      }
    }


    val inputLines = input.filter(_.nonEmpty) // filtteröidään tyhjät rivit pois, jäljelle jää string muotoisia muuttujia
      .map(_.split(" -> ")) // splitataa string muotoiset rivit yksittäisiksi numeroiksi, jokaisesta rivistä tulee array
      .map(_.map(_.split(",")))
      .map(_.map(_.map(_.toInt))) // muutetaan stringit numeroiksi

    inputLines.foreach(addLine(_))
    val result = matrix.map(_.count(_ > 1))

    println("määrä on " + result.sum)
    // 82274 liian paljon
  }
}
