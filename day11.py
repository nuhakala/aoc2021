from functools import reduce
from operator import add


with open ('./inputs/day11_in.txt', 'r') as tiedosto:
    DATA = {(x, y):int(d) for (y, r) in enumerate(tiedosto.readlines()) for (x, d) in enumerate(r.strip())}

#                     ylärivi              keskirivi              alarivi
NAABURIT = ((-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1))

def printtaa(taulukko):
    for (x, y) in taulukko:
        print(taulukko[(x, y)], end=" ")
        if (x == 9): print()
    print()

# valayttaa naapurit ja rekursiivisesti jatkaa niin kauan kun väläytttävää riittää
def valayta_naaburit(x, y, taulukko):
    maara = 0
    for (xx, yy, arvo) in ((x + tx, y + ty, taulukko.get((x + tx, y + ty), -1)) for (tx, ty) in NAABURIT):
        # arvon pitää olla suurempaa kuin nolla, koska jos se on nolla, niin se on jo välähtänyt tällä kierroksella
        if (arvo > 0):
            # jos taulukosta haettu arvo on 9 niin silloin se kasvaa kymppiin ja se väläytetään
            if (arvo >= 9):
                maara += 1
                taulukko[(xx, yy)] = 0
                maara += valayta_naaburit(xx, yy, taulukko)
            else:
                taulukko[(xx, yy)] += 1

    return maara

valahdykset = 0
kehitys = DATA.copy()
kierros_100 = 0
askel = 0
while True:
    askel += 1
    # nostetaan jokaisen mustekalan tasoa
    for (x, y) in DATA:
        kehitys[(x, y)] += 1
    # sen jälkeen väläytetään jokaista
    for (x, y) in DATA:
        if (kehitys[(x, y)] > 9):
            kehitys[(x, y)] = 0
            valahdykset += 1
            valahdykset += valayta_naaburit(x, y, kehitys)
            if (askel == 100): kierros_100 = valahdykset
    if all(0 == arvo for arvo in kehitys.values()): break


print("välähdyksiä yhteensä 100 askeleen aikana: ", end="")
print(kierros_100)
print("kaikki ovat nollaa kierroksella: ", end="")
print(askel)
# 674 liian vähän
