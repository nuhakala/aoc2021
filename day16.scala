import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer
import scala.annotation.switch

object app {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day16_in.txt")
    val lines = new BufferedReader(file)

    val strs = LazyList.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    val hex = input.head

    var bin = ""
    for (kirjain <- hex) {
      val add = "0000" + Integer.parseInt(kirjain.toString, 16).toBinaryString
      bin = bin + add.takeRight(4)
    }

    val res = paketti(bin)
    println("versiosumma on: " + res._1)
    println("laskutoimituksen lopputulos on: " + res._2)
  }

  def paketti(bin: String): (Int, Long, String) = {
    var muokattu = bin
    val ver = Integer.parseInt(muokattu.take(3), 2)
    muokattu = muokattu.drop(3)
    val typ = Integer.parseInt(muokattu.take(3), 2)
    muokattu = muokattu.drop(3)
    val typeLength = Integer.parseInt(muokattu.take(1), 2)
    if (typ != 4) muokattu = muokattu.drop(1)
    var length = 0
    var versio = ver
    var res = 0L
    val pak = Buffer[Long]()

    // käydään rekursiivisesti paketit läpi ja tallennetaan ne pak muuttujaan
    // lasketaan myös samalla ykkös tehtävän versiosumma
    if (typ != 4) {
      if (typeLength == 1) {
        length = Integer.parseInt(muokattu.take(11), 2)
        muokattu = muokattu.drop(11)
        for (i <- (0 until length)) {
          val tupla = paketti(muokattu)
          versio += tupla._1
          muokattu = tupla._3
          pak += tupla._2
        }
      } else {
        length = Integer.parseInt(muokattu.take(15), 2)
        muokattu = muokattu.drop(15)
        val paketit = muokattu.take(length)
        val vanha = muokattu.length
        while (muokattu.length > vanha - length) {
          val tupla = paketti(muokattu)
          versio += tupla._1
          muokattu = tupla._3
          pak += tupla._2
        }
      }
    }

    // sitten suoritetaan laskutoimitukset tyyppi muuttujan perusteella
    typ match {
      case 0 =>
        res = pak.sum

      case 1 =>
        res = pak.product

      case 2 =>
        res = pak.min

      case 3 =>
        res = pak.max

      case 4 => 
        var numero = ""
        var eka = muokattu.head
        while (eka == '1') {
          muokattu = muokattu.drop(1)
          numero = numero + muokattu.take(4)
          muokattu = muokattu.drop(4)
          eka = muokattu.head
        }
        muokattu = muokattu.drop(1)
        numero = numero + muokattu.take(4)
        muokattu = muokattu.drop(4)
        res = scala.math.BigInt(numero, 2).toLong

      case 5 =>
        if (pak.head > pak(1)) res = 1
        else res = 0

      case 6 =>
        if (pak(0) < pak(1)) res = 1
        else res = 0

      case 7 =>
        if (pak(0) == pak(1)) res = 1
        else res = 0

      case _ => 
    }

    return (versio, res, muokattu)
  }
}
