import math
from queue import PriorityQueue

class Vertex:
    def __init__(self, weight, location, distance, predecessor):
        """
        Alustaa solmun.
        weight = int = numero paikassa location
        location = (x, y) = solmun sijainti ruudukossa
        distance = int = polun painojen suurimman
        predecessor = (x, y) = solmua edeltävä solmu
        """
        self.w = weight
        self.loc = location
        self.dist = distance
        self.pre = predecessor
        self.x = location[0]
        self.y = location[1]

    def copy(self):
        return Vertex(self.w, self.loc, self.dist, self.pre)

def relax(u: Vertex, v: Vertex):
    if u.dist + v.w < v.dist:
        v.dist = u.dist + v.w
        v.pre = u.loc

with open ('./inputs/day15_in.txt', 'r') as tiedosto:
    DATA = {(x + 1, y + 1): Vertex(int(d), (x + 1, y + 1), math.inf, (-1, -1)) for (y, r) in enumerate(tiedosto.readlines()) for (x, d) in enumerate(r.strip())}
    # weight, (x, y), distance, predecessor

koko = int(math.sqrt(len(DATA)))

naapurit = [(1, 0), (-1, 0), (0, 1), (0, -1)]
q = PriorityQueue()
processed = set({})
t1 = {tupla: DATA[tupla].copy() for tupla in DATA}
node = t1[(1, 1)]
node.dist = 0
q.put((node.dist, node.w, node))

# i tarvitaan, koska monella solmulla voi olla sama distance --> silloin priorityqueue järjestää
# tuplen toisen argumentin perusteella --> jos ei ole i:tä, niin heittää erroria että 
# Vertex ja Vertex ei voi verrata > operaattorilla
i = 0
while not q.empty():
    u = q.get()[2]
    if u in processed: continue
    processed.add(u)
    for naapuri in naapurit:
        i += 1
        v = t1.get((u.x + naapuri[0], u.y + naapuri[1]))
        if v:
            relax(u, v)
            q.put((v.dist, i, v))

print("t1 vastaus: ", t1[(koko, koko)].dist)


# ********
# osa kaksi
# ********

t2 = {}
# alustetaan t2 mappi: kopioidaan siitä 5x5 gridi ja kasvatetaan solmujen weighttiä
for xx in range(5):
    for yy in range(5):
        for (x, y) in DATA:
            node = DATA[(x, y)].copy()
            node.w += xx + yy
            node.x = x + xx * koko
            node.y = y ++ yy * koko
            node.loc = (x + xx * koko, y + yy * koko)
            if node.w > 9: node.w = node.w % 9
            t2[(x + xx * koko, y + yy * koko)] = node


# alustetaan uudestaan arvot ja käydään sama looppu läpi uudella matriisilla
processed = set({})
solmu = t2[(1, 1)]
solmu.dist = 0
q = PriorityQueue()
q.put((solmu.dist, solmu.w, solmu))

i = 0
while not q.empty():
    u = q.get()[2]
    if u in processed: continue
    processed.add(u)
    for naapuri in naapurit:
        i += 1
        v = t2.get((u.x + naapuri[0], u.y + naapuri[1]))
        if v:
            relax(u, v)
            q.put((v.dist, i, v))

print("t2 vastaus: ", t2[(5 * koko, 5 * koko)].dist)
