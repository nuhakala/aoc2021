#include <stdio.h>
#include <stdlib.h>

int lueLuku(FILE *sana);

int main(void) {
  FILE *fp;
  fp = fopen("./inputs/day17_in.txt", "r");

  int xs, xe, ys, ye;
  char ch;

  /* Nojoo, vähän toistaa nää if lauseet täs silmukas toisiaan mut ei anneta sen häiritä. */
  while((ch = fgetc(fp)) != EOF) {
    if(ch == 'x') {
      /* skipataan yhtäsuuruusmerkki */
      fgetc(fp);

      /* sitten luetaan itse luku */
      xs = lueLuku(fp);
      /* luetaan toinenkin piste pois */
      fgetc(fp);
      xe = lueLuku(fp);
    }
    if(ch == 'y') {
      /* skipataan yhtäsuuruusmerkki */
      fgetc(fp);

      /* sitten luetaan itse luku */
      ys = lueLuku(fp);
      /* luetaan toinenkin piste pois */
      fgetc(fp);
      ye = lueLuku(fp);
    }
  }
  fclose(fp);

  
  /* Eliggäns, kysytään isointa mahdollista y paikkaa kaikilla radoilla jotka päätyvät kohde
  * alueelle. No, vähän ku miettii niin kun lähdetään nollasta ja kiihtyvyys tippuu aina yhdellä
  * niin silloin alaspäin tullessa proben y-tasot ovat täysin samat kuin ylöspäin mennessä.
  * Tästä seuraa että suurin mahdollinen kiihtyvyys on ys - 1, koska alaspäin mennessä osutaan
  * 0-tasolle ja siitä sitte yks pykälä alaspäin niin osutaan just kohdealueen alarajalle.
  * Eli suurin y-taso saadaan ku summataan vaan ylöspäin mennessä tasot. */ 
  int yMax = 0;
  for (int i = 0; i < abs(ys); i++) {
    yMax += i;
  }

  printf("Suurin y-taso: %d\n", yMax);


  /* kakkos osio alkaa tästä!!! */

  int kaikki = 0;
  /* Käydään loopissa läpittä kaikki mahdolliset vaihtoehdot. */
  for (int i = 0; i < 300; i++) {
    for (int a = -200; a < 200; a++) {
      /* Alustetaan kiihtyvyydet ja paikat */
      int yv = a;
      int xv = i;
      int y = 0;
      int x = 0;

      /* Lasketaan rataa niin kauan kunnes se ei ole enää mahdollinen */
      while (y >= ys) {
        y += yv;
        x += xv;
        /* Jos on päästy kohdealueelle, niin lisätään summaa yhdellä ja päätetään tämä kierros tähän. */
        if(y >= ys && y <= ye && x >= xs && x <= xe) {
          kaikki++;
          break;
        }

        /* Päivitetään kiihtyvyydet. */
        if(xv > 0) {
          xv--;
        }
        yv--;
      }
    }
  }

  printf("Kohdealueelle päätyvien ratojen määrä: %d\n", kaikki);
  return 0;
}


/* Lukee niputista luvun ja muuntaa sen int-tyyppiseksi muuttujaksi ja palauttaa sen. */
int lueLuku(FILE *fp) {
  char luku[5] = {0};
  int i = 0;
  char ch;
  /* pysäytetään luku pisteeseen, pilkkuun tai siihen että tiedosto loppuu */
  while((ch = fgetc(fp)) != '.' && ch != ',' && ch != EOF) {
    luku[i] = ch;
    i++;
  }
  return atoi(luku);
}
