import java.io.{BufferedReader, FileNotFoundException, FileReader}
import scala.collection.mutable.Map
import scala.collection.mutable.Buffer

object Day8 {
  def main(args: Array[String]): Unit = {
    val file = new FileReader("./inputs/day8_in.txt")
    val lines = new BufferedReader(file)

    val strs = LazyList.continually(lines.readLine()).takeWhile(_ != null)
    val input = strs.toVector

    file.close()
    lines.close()

    val rows = input.map(_.split(" "))
      .map(_.filter(_ != "|"))

    val divided = rows.map(_.splitAt(10))

    var count = 0
    for ((in, out) <- divided) {
      count += out.count(st => st.size < 5 || st.size == 7)
    }

    println("ekan tehtävän vastaus: " + count)

    // *******
    // toinen osa
    // *******


    val result = Buffer[String]()
    count = 0

    for (line <- rows) {

      val row = line.map(_.sorted).distinct
      val one = row.find(_.size == 2).get
      val seven = row.find(_.size == 3).get
      val four = row.find(_.size == 4).get
      val eight = row.find(_.size == 7).get

      val bottomLeft = eight.filterNot(char => four.contains(char) || seven.contains(char))
      val sixTwoZero = row.filter(word => word.contains(bottomLeft(0)) && word.contains(bottomLeft(1))).filterNot(_.size == 7)
      val zero = sixTwoZero.filter(word => word.contains(one(0)) && word.contains(one(1))).head
      val threeNineZero = row.filter(word => word.contains(seven(0)) && word.contains(seven(1)) && word.contains(seven(2)))
        .filterNot(word => word.size < 5 || word.size > 6)
      val threeNine = threeNineZero.filterNot(word => word == zero)
      val nine = threeNine.filter(_.size == 6).head
      val three = threeNine.filter(_.size == 5).head
      val sixNineZero = row.filter(_.size == 6)
      val six = sixNineZero.filterNot(word => word == nine || word == zero).head
      val fiveTwoThree = row.filter(_.size == 5)
      val fiveTwo = fiveTwoThree.filterNot(_ == three)
      val topRight = (eight.toBuffer --= six.toBuffer).head
      val five = fiveTwo.filterNot(_.contains(topRight)).head
      val two = fiveTwo.filter(_.contains(topRight)).head

      val divid = line.map(_.sorted).splitAt(10)
      var res = ""
      divid._2.foreach( word => {
        word match {
          case a if(a == zero) => res = res + "0"
          case b if(b == one) => res = res + "1"
          case c if(c == two) => res = res + "2"
          case d if(d == three) => res = res + "3"
          case e if(e == four) => res = res + "4"
          case f if(f == five) => res = res + "5"
          case g if(g == six) => res = res + "6"
          case h if(h == seven) => res = res + "7"
          case i if(i == eight) => res = res + "8"
          case j if(j == nine) => res = res + "9"
          case _ =>
        }
      } )


      //result += jotai.foldLeft("")((word, next) => word + next)
      //println(count + " " + res)
      count += 1
      result += res
    }
    val sum = result.map(_.toInt).sum
    println("tokan tehtävän vastaus: " + sum)
    // 604828 liian vähän

  }
}
